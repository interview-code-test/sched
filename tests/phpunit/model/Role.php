<?php
namespace SCHEDORG\Test\PHPUnit\Models;

use \PHPUnit\Framework\TestCase;
use SCHEDORG\Models\Role;

class RoleTest extends TestCase
{
    protected $stack;

    public function setUp()
    {
        $this->stack['role'] = 'speaker';
    }

    public function testSetterAndGetter()
    {
        $role = new Role();
        $role->setRole($this->stack['role']);

        $this->assertSame($role->getRole(), $this->stack['role']);
    }
}
