<?php
namespace SCHEDORG\Test\PHPUnit\Models;

use \PHPUnit\Framework\TestCase;
use SCHEDORG\Models\Event;
use SCHEDORG\Models\Role;
use SCHEDORG\Models\Session;
use SCHEDORG\Models\User;

class EventTest extends TestCase
{
    protected $stack;

    public function setUp()
    {
        $this->stack['role_name'] = 'speaker';
        $this->stack['role'] = new Role($this->stack['role_name']);
        $this->stack['user'] = new User();
        $session = new Session();
        $session->setId('143a124eca93d53f277fe2f74fb95e80');
        $this->stack['session'] = $session;
    }

    public function testSetterAndGetter()
    {
        $event = new Event();
        $event->addSession($this->stack['session']);

        $this->assertContainsOnlyInstancesOf('SCHEDORG\Models\Session', $event->getSessions());
    }
}
