<?php
namespace SCHEDORG\Test\PHPUnit\Models;

use \DateTime;
use \PHPUnit\Framework\TestCase;
use SCHEDORG\Models\Session;
use SCHEDORG\Models\User;

class SessionTest extends TestCase
{
    protected $stack;

    public function setUp()
    {
        $this->stack['id'] = '143a124eca93d53f277fe2f74fb95e80';
        $this->stack['name'] = 'Strange Robotics';
        $this->stack['start'] = new DateTime('1/25/2019 15:30:00');
        $this->stack['end'] = new DateTime('1/25/2019 16:30:00');

        $speaker1 = new User();
        $speaker1->setFirstName('John');
        $this->stack['speaker1'] = $speaker1;
        $speaker2 = new User();
        $speaker2->setFirstName('Wayne');
        $this->stack['speaker2'] = $speaker2;
    }

    public function testSetterAndGetter()
    {
        $session = new Session();
        $session->setId($this->stack['id']);
        $session->setName($this->stack['name']);
        $session->setStartDate($this->stack['start']);
        $session->setEndDate($this->stack['end']);
        $session->addSpeaker($this->stack['speaker1']);

        $this->assertSame($session->getId(), $this->stack['id']);
        $this->assertSame($session->getName(), $this->stack['name']);
        $this->assertSame($session->getStartDate(), $this->stack['start']);
        $this->assertSame($session->getEndDate(), $this->stack['end']);
        $this->assertContainsOnlyInstancesOf('SCHEDORG\Models\User', $session->getSpeakers());
    }

    public function testPrimarySpeaker()
    {
        $session = new Session();
        $session->setId($this->stack['id']);
        $session->addSpeaker($this->stack['speaker1']);
        $session->addSpeaker($this->stack['speaker2']);

        $this->assertSame('John', $session->getPrimarySpeaker()->getFirstName());
    }
}
