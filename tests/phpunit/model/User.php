<?php
namespace SCHEDORG\Test\PHPUnit\Models;

use \PHPUnit\Framework\TestCase;
use SCHEDORG\Models\Role;
use SCHEDORG\Models\User;

class UserTest extends TestCase
{
    protected $stack;

    public function setUp()
    {
        $this->stack['role_name'] = 'speaker';
        $this->stack['role'] = new Role($this->stack['role_name']);
        $this->stack['first_name'] = 'Sally';
        $this->stack['middle_name'] = '';
        $this->stack['last_name'] = 'Ride';
    }

    public function testSetterAndGetter()
    {
        $user = new User();
        $user->addRole($this->stack['role']);
        $user->setFirstName($this->stack['first_name']);
        $user->setMiddleName($this->stack['middle_name']);
        $user->setLastName($this->stack['last_name']);

        $this->assertContains($this->stack['role'], $user->getRoles());
        $this->assertSame($user->getFirstName(), $this->stack['first_name']);
        $this->assertEmpty($user->getMiddleName());
        $this->assertSame($user->getLastName(), $this->stack['last_name']);
    }

    public function testUserHasRole()
    {
        $role = new Role();
        $role->setRole($this->stack['role_name']);

        $user = new User();
        $user->addRole($role);

        $this->assertTrue($user->hasRole($this->stack['role_name']));
    }
}
