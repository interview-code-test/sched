<?php
namespace SCHEDORG\Test\PHPUnit\Utils;

use \PHPUnit\Framework\TestCase;
use SCHEDORG\Utils\NameUtils;

class NameUtilsTest extends TestCase
{
    public function testParseFullName()
    {
        $name = 'Miles Davis';
        list($firstName, $middleName, $lastName) = NameUtils::split($name);

        $this->assertSame('Miles', $firstName);
        $this->assertSame('', $middleName);
        $this->assertSame('Davis', $lastName);
    }

    public function testApostrophesAllowed()
    {
        $name = "James O'Reilly";
        list($firstName, $middleName, $lastName) = NameUtils::split($name);

        $this->assertSame('James', $firstName);
        $this->assertSame('', $middleName);
        $this->assertSame("O'Reilly", $lastName);
    }

    public function testSpecialCharactersNotAllowed()
    {
        $name = 'Test (Ryan)';
        list($firstName, $middleName, $lastName) = NameUtils::split($name);

        $this->assertSame('Test', $firstName);
        $this->assertSame('', $middleName);
        $this->assertSame('Ryan', $lastName);
    }

    public function testMiddleName()
    {
        $name = 'Blah Blah 1';
        list($firstName, $middleName, $lastName) = NameUtils::split($name);

        $this->assertSame('Blah', $firstName);
        $this->assertSame('Blah', $middleName);
        $this->assertSame('', $lastName);
    }

    public function testMoreThan3Words()
    {
        $name = 'Julia Scarlett Elizabeth Louis-Dreyfus';
        list($firstName, $middleName, $lastName) = NameUtils::split($name);

        $this->assertSame('Julia', $firstName);
        $this->assertSame('Scarlett', $middleName);
        $this->assertSame('Louis-Dreyfus', $lastName);
    }

    public function test1Word()
    {
        $name = 'Nasa';
        list($firstName, $middleName, $lastName) = NameUtils::split($name);

        $this->assertSame('Nasa', $firstName);
        $this->assertSame('', $middleName);
        $this->assertSame('', $lastName);
    }
}
