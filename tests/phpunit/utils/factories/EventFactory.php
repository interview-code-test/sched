<?php
namespace SCHEDORG\Test\PHPUnit\Utils\Factories;

use \PHPUnit\Framework\TestCase;
use SCHEDORG\Utils\Factories\EventFactory;

class EventFactoryTest extends TestCase
{
    protected $stack;

    public function setUp()
    {
        $this->stack['start'] = '2019-01-25 15:30:00';
        $this->stack['end'] = '2019-01-25 16:30:00';
        $this->stack['range'] = 'Friday, Jan 25 2019 - 03:30 pm to 04:30 pm';
    }

    public function testCreateDataIterator()
    {
        $iterator = EventFactory::createDataIterator(__DIR__ . '/../../../../data/user.csv');

        $this->assertInstanceOf('Iterator', $iterator);
    }

    public function testCreateDataService()
    {
        $this->assertInstanceOf('SCHEDORG\Services\EventDataService', EventFactory::createDataService());
    }

    public function testCreateScheduler()
    {
        $this->assertInstanceOf('SCHEDORG\Controllers\Scheduler', EventFactory::createScheduler());
    }

    public function testCreateEvent()
    {
        $this->assertInstanceOf('SCHEDORG\Models\Event', EventFactory::createEvent());
    }

    public function testCreateSession()
    {
        $data = [];
        $data['id'] = '143a124eca93d53f277fe2f74fb95e80';
        $data['name'] = 'Strange Robotics';
        $data['session_start'] = '1/25/2019 15:30:00';
        $data['session_end'] = '1/25/2019 16:30:00';

        $this->assertInstanceOf('SCHEDORG\Models\Session', EventFactory::createSession($data));
    }

    public function testCreateUser()
    {
        $data = [];
        $data['id'] = '123456';
        $data['name'] = 'Doraemon';

        $this->assertInstanceOf('SCHEDORG\Models\User', EventFactory::createUser($data));
    }

    public function testCreateRole()
    {
        $this->assertInstanceOf('SCHEDORG\Models\Role', EventFactory::createRole());
    }
}
