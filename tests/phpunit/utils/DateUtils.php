<?php
namespace SCHEDORG\Test\PHPUnit\Utils;

use \DateTime;
use \PHPUnit\Framework\TestCase;
use SCHEDORG\Utils\DateUtils;

class DateUtilsTest extends TestCase
{
    protected $stack;

    public function setUp()
    {
        $this->stack['start'] = '2019-01-25 15:30:00';
        $this->stack['end'] = '2019-01-25 16:30:00';
        $this->stack['range'] = 'Friday, Jan 25 2019 - 03:30 pm to 04:30 pm';
    }

    public function testRange()
    {
        $start = new DateTime($this->stack['start']);
        $end = new DateTime($this->stack['end']);

        $range = DateUtils::range($start, $end);

        $this->assertSame($range, $this->stack['range']);
    }
}
