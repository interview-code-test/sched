<?php
namespace SCHEDORG\Test\PHPUnit\Utils\Sorters;

use \PHPUnit\Framework\TestCase;
use SCHEDORG\Models\Event;
use SCHEDORG\Models\Role;
use SCHEDORG\Models\User;
use SCHEDORG\Models\Session;
use SCHEDORG\Utils\Sorters\SortSessionsBySpeakersFirstName;

class SortSpeakersByLastNameTest extends TestCase
{
    protected $stack;

    public function setUp()
    {
        $user1 = new User();
        $user1->setFirstName('Amy');
        $this->stack['user1'] = $user1;
        $user2 = new User();
        $user2->setFirstName('Bob');
        $this->stack['user2'] = $user2;
        $user3 = new User();
        $user3->setFirstName('Charlie');
        $this->stack['user3'] = $user3;
        
        $this->stack['session1_id'] = 'f735a43e6933e41901887035236e38f1';
        $this->stack['session1_name'] = 'Anonymous Callers';
        $this->stack['session2_id'] = 'f735a43e6933e41901887035236e38f2';
        $this->stack['session2_name'] = 'Best Friends Forever';
        
        $session1 = new Session();
        $session1->setId($this->stack['session1_id']);
        $session1->setName($this->stack['session1_name']);
        $session1->addSpeaker($user2);
        $this->stack['session1'] = $session1;

        $session2 = new Session();
        $session2->setId($this->stack['session2_id']);
        $session2->setName($this->stack['session2_name']);
        $session2->addSpeaker($user1);
        $session2->addSpeaker($user3);
        $this->stack['session2'] = $session2;

        $event = new Event();
        $event->addSession($this->stack['session1']);
        $event->addSession($this->stack['session2']);
        $this->stack['event'] = $event;
    }

    public function testSort()
    {
        $event = $this->stack['event'];
        $sorter = new SortSessionsBySpeakersFirstName();

        $sessions = $event->getSessions();
        $session1 = $sessions[$this->stack['session1_id']];
        $this->assertSame($session1->getName(), $this->stack['session1_name']);
        
        $sessions = $sorter->sort($sessions);
        
        $this->assertSame($sessions[0]->getName(), $this->stack['session2_name']);
    }
}
