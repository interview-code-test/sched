<?php
/**
 * Scheduler.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Controllers;

use SCHEDORG\Services\EventService;
use SCHEDORG\Services\IDataService;
use SCHEDORG\Utils\Sorters\ISort;
use SCHEDORG\Utils\DateUtils;

/**
 * Scheduler acts as a controller to collect and display an Event's information.
 *
 * The Session name and time along with speakers are displayed.
 *
 * @package     schedorg/chip
 * @subpackage  controllers
 */
class Scheduler
{
    /**
     * The Event model.
     *
     * @var Event
     */
    private $event;

    /**
     * Data Service for the event.
     *
     * @var IDataService
     */
    private $dataService;

    /**
     * Sorter.
     *
     * @var ISort
     */
    private $sortSessionsBySpeakersFirstName;

    /**
     * Constructor.
     *
     * @param IDataService $dataService
     * @param ISort $sortSessionsBySpeakersFirstName
     */
    public function __construct(
        IDataService $dataService,
        ISort $sortSessionsBySpeakersFirstName
    ) {
        $this->dataService = $dataService;
        $this->sortSessionsBySpeakersFirstName = $sortSessionsBySpeakersFirstName;
    }

    /**
     * Collects an Event data.
     *
     * @return void
     */
    public function run()
    {
        $this->event = $this->dataService->get();

        return $this;
    }

    /**
     * Gets the Sessions, sorted by speakers last names.
     *
     * @return array
     */
    public function getSessions()
    {
        return $this->sortSessionsBySpeakersFirstName->sort($this->event->getSessions());
    }

    /**
     * Displays JSON formatted Sessions data.
     *
     * @return self
     */
    public function displayJSON()
    {
        echo json_encode($this->getSessions(), JSON_FORCE_OBJECT);

        return $this;
    }

    /**
     * Returns human-readable array of Sessions data
     *
     * @return self
     */
    public function dump()
    {
        print_r($this->getSessions());

        return $this;
    }

    /**
     * Displays formatted Sessions data.
     *
     * @return self
     */
    public function out()
    {
        $counter = 0;

        foreach ($this->getSessions() as $session) {
            echo "\n\n" . ++$counter . '. Session Name: ' . htmlspecialchars($session->getName()) . "\n"
                . DateUtils::range($session->getStartDate(), $session->getEndDate())
                . ' (' . DateUtils::duration($session->getStartDate(), $session->getEndDate()) . ')'
                . "\nSpeakers:\n";
        
            foreach ($session->getSpeakers() as $speaker) {
                $middleName = $speaker->getMiddleName();
                $lastName = $speaker->getLastName();

                echo "- {$speaker->getFirstName()} "
                    . ($middleName ?? '')
                    . ' '
                    . ($lastName ?? '')
                    . "\n";
            }
        }

        return $this;
    }

    /**
     * Displays Sessions data in HTML.
     *
     * @return self
     */
    public function render()
    {
        $counter = 0;

        echo '<ol>';

        foreach ($this->getSessions() as $session) {
            echo '<li><p><strong>' . htmlspecialchars($session->getName()) . '</strong></p>'
                . '<p>' . DateUtils::range($session->getStartDate(), $session->getEndDate())
                . ' (' . DateUtils::duration($session->getStartDate(), $session->getEndDate()) . ')'
                . '<p><strong>Speakers:</strong></p>'
                . '<ol>';

            foreach ($session->getSpeakers() as $speaker) {
                $middleName = $speaker->getMiddleName();
                $lastName = $speaker->getLastName();

                echo "<li>{$speaker->getFirstName()} "
                    . ($middleName ?? '')
                    . ' '
                    . ($lastName ?? '')
                    . '</li></li>';
            }

            echo '</ol>';
        }

        echo '</ol>';

        return $this;
    }
}
