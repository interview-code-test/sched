<?php
/**
 * IDataService.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Services;

/**
 * Data Service Interface.
 *
 * @package     schedorg/chip
 * @subpackage  services
 */
interface IDataService
{
    public function get();
}
