<?php
/**
 * EventDataService.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Services;

use \Iterator;
use SCHEDORG\Services\IDataService;
use SCHEDORG\Utils\Factories\EventFactory;
use SCHEDORG\Models\Event;

/**
 * Event Data Service.
 *
 * Collects Sessions data and find their Speakers. Found Session and User is cached on run-time
 * to minimize the operation of interacting with data source (csv files).
 *
 * @package     schedorg/chip
 * @subpackage  services
 */
class EventDataService implements IDataService
{
    /**
     * Cache list of found Users.
     *
     * @var array
     */
    private $users = [];

    /**
     * Cache list of found Sessions.
     *
     * @var array
     */
    private $sessions = [];

    /**
     * Constructor.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Opens CSV files and iterates through the join table (role.csv) and finds a matching
     * Session (session.csv) and User (user.csv). A Session must be active and the User Type
     * must be speaker. Returns an Event model containing a list of Sessions containing a list of Speakers.
     *
     * @return Event
     */
    public function get()
    {
        // Opens these data source csv files.
        $dataSource = ['role', 'session', 'user'];

        // Uses {@see CSVFileIterator} to parse the files.
        foreach ($dataSource as $fileName) {
            $iteratorName = $fileName . 'Iterator';
            $$iteratorName = EventFactory::createDataIterator(SCHEDORG_DATA_DIR . $fileName . '.csv');
        }

        $headers = [];

        // Starts iterating the join table.
        while ($roleIterator->valid()) {
            // Uses the headers to denote each entry on a line inside the file.
            if (empty($headers)) {
                $headers = $roleIterator->current();
                $roleIterator->next();
                continue;
            }
            
            $roleEntries = array_combine($headers, $roleIterator->current());

            // Skips other Roles othen than "speaker" user type.
            if ('speaker' === $roleEntries['usertype']) {
                // Finds active session.
                $sessionEntries = $this->fetch(
                    $roleEntries['sessionid'],
                    $this->sessions,
                    $sessionIterator,
                    function ($entry) use ($roleEntries) {
                        return ('Y' === $entry['active']) && ($roleEntries['sessionid'] === $entry['id']);
                    }
                );

                if ($sessionEntries) {
                    $this->sessions[$sessionEntries['id']] = $sessionEntries;

                    // Finds Speakers for the Session.
                    $userEntries = $this->fetch(
                        $roleEntries['userid'],
                        $this->users,
                        $userIterator,
                        function ($entry) use ($roleEntries) {
                            return $entry['id'] === $roleEntries['userid'];
                        }
                    );

                    if ($userEntries) {
                        $this->users[$userEntries['id']] = $userEntries;

                        // Makes unique entry for each Session into an Event.
                        if (!$this->event->exists($sessionEntries['id'])) {
                            $this->event->addSession(EventFactory::createSession($sessionEntries));
                        }

                        $this->event->addSpeaker($sessionEntries['id'], EventFactory::createSpeaker($userEntries));
                    }
                }
            }

            $roleIterator->next();
        }

        return $this->event;
    }

    /**
     * Checks cached data and returns it if it exists. Otherwise, the data source will be used.
     *
     * @param string $needle
     * @param array $haystack
     * @param Iterator $iterator
     * @param callable $condition
     * @return array
     */
    private function fetch($needle, $haystack, Iterator $iterator, callable $condition)
    {
        $record = array_key_exists($needle, $haystack) ? $haystack[$needle] : [];

        if (empty($record)) {
            $record = $this->findOne($iterator, $condition);
        }

        return $record;
    }

    /**
     * Iterates through a data source and finds a matching record based on the condition passed.
     *
     * @param \Iterator $iterator
     * @param callable $condition
     * @return void
     */
    private function findOne(\Iterator $iterator, callable $condition)
    {
        $headers = [];
        $record = null;

        $iterator->rewind();

        while ($iterator->valid()) {
            if (empty($headers)) {
                $headers = $iterator->current();
                $iterator->next();
                continue;
            }

            $entries = array_combine($headers, $iterator->current());

            if ($condition($entries)) {
                $record = $entries;
                break;
            }

            $iterator->next();
        }

        return $record;
    }
}
