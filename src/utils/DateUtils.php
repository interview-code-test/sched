<?php
/**
 * DateUtils.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Utils;

use \DateTime;

/**
 * Date Utility.
 *
 * @package     schedorg/chip
 * @subpackage  utils
 */
class DateUtils
{
    /**
     * Formats the start to end time. The end date will not be shown if it's on the same day
     * with the start time.
     *
     * @param DateTime $start
     * @param DateTime $end
     * @return void
     */
    public static function range(DateTime $start, DateTime $end)
    {
        $format = 'l, M d Y - h:i a';
        $interval = $end->diff($start)->format('%d');

        $finishFormat = ('0' === $interval) ? 'h:i a' : $format;

        return $start->format($format) . ' to ' . $end->format($finishFormat);
    }

    /**
     * Formats the length between the start and end time in the duration of hours.
     *
     * @param DateTime $start
     * @param DateTime $end
     * @return void
     */
    public static function duration(DateTime $start, DateTime $end)
    {
        return $end->diff($start)->format('%h hour(s)');
    }
}
