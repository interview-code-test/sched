<?php
/**
 * EventFactory.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Utils\Factories;

use \DateTime;
use SCHEDORG\Services\EventDataService;
use SCHEDORG\Utils\Iterators\CSVFileIterator;
use SCHEDORG\Controllers\Scheduler;
use SCHEDORG\Models\Event;
use SCHEDORG\Models\Session;
use SCHEDORG\Models\User;
use SCHEDORG\Utils\NameUtils;
use SCHEDORG\Models\Role;
use SCHEDORG\Utils\Sorters\SortSessionsBySpeakersFirstName;
use SCHEDORG\Utils\Sorters\SortSpeakersByLastName;

/**
 * Event Factory
 *
 * Creates associated objects for building an Event.
 *
 * @package     schedorg/chip
 * @subpackage  utils/factories
 */
class EventFactory
{
    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $fileName
     * @return CSVFileIterator
     */
    public static function createDataIterator($fileName)
    {
        return new CSVFileIterator($fileName);
    }

    /**
     * @return IDataService
     */
    public static function createDataService()
    {
        return new EventDataService(self::createEvent());
    }

    /**
     * @return Scheduler
     */
    public static function createScheduler()
    {
        return new Scheduler(
            self::createDataService(),
            new SortSessionsBySpeakersFirstName
        );
    }

    /**
     * @return Event
     */
    public static function createEvent()
    {
        return new Event();
    }

    /**
     * @param array $data
     * @return Session
     */
    public static function createSession(array $data)
    {
        $session = new Session();
        $session->setId($data['id'])
            ->setName($data['name'])
            ->setStartDate(new DateTime($data['session_start']))
            ->setEndDate(new DateTime($data['session_end']));

        return $session;
    }

    /**
     * @param array $data
     * @return User
     */
    public static function createUser(array $data)
    {
        list($firstName, $middleName, $lastName) = NameUtils::split($data['name']);

        $user = new User();
        $user->setId($data['id'])
            ->setFirstName($firstName)
            ->setMiddleName($middleName)
            ->setLastName($lastName);

        return $user;
    }

    /**
     * @param array $data
     * @return User
     */
    public static function createSpeaker(array $data)
    {
        $user = self::createUser($data);
        $role = self::createRole();
        $role->setRole('speaker');
        $user->addRole($role);

        return $user;
    }

    /**
     * @return Role
     */
    public static function createRole()
    {
        return new Role();
    }
}
