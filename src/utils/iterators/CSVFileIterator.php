<?php
/**
 * CSVFileIterator.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Utils\Iterators;

/**
 * CSV File Iterator.
 *
 * Iterates a csv file. Adapted from {@link https://phpunit.readthedocs.io/en/7.1/writing-tests-for-phpunit.html}
 *
 * @package     schedorg/chip
 * @subpackage  utils/iterators
 */
class CSVFileIterator implements \Iterator
{
    /**
     * File pointer.
     *
     * @var resource
     */
    protected $filePointer;

    /**
     * The position of the file pointer in seeking the file.
     *
     * @var integer
     */
    protected $key = 0;

    /**
     * Current row the file pointer is seeking at.
     *
     * @var array
     */
    protected $current;

    /**
     * Contructor.
     *
     * @param string $fileName
     */
    public function __construct($fileName)
    {
        $this->filePointer = fopen($fileName, 'r');
    }

    /**
     * Destructor.
     */
    public function __destruct()
    {
        if (!empty($this->filePointer)) {
            fclose($this->filePointer);
        }
    }

    /**
     * Starts from the top of the file.
     *
     * @return null
     */
    public function rewind()
    {
        rewind($this->filePointer);
        $this->current = fgetcsv($this->filePointer);
        $this->key = 0;
    }

    /**
     * Checks for more row.
     *
     * @return bool
     */
    public function valid()
    {
        return !feof($this->filePointer);
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * @return array
     */
    public function current()
    {
        return $this->current;
    }

    /**
     * Advances the file pointer to the next available record.
     *
     * @return null
     */
    public function next()
    {
        $this->current = fgetcsv($this->filePointer);
        $this->key++;
    }
}
