<?php
/**
 * SortSessionsBySpeakersFirstName.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Utils\Sorters;

use SCHEDORG\Models\Event;

/**
 * Sort the Sessions by the Speaker's first name.
 *
 * @package     schedorg/chip
 * @subpackage  utils/sorters
 */
class SortSessionsBySpeakersFirstName implements ISort
{
    /**
     * Sorts Sessions by the Primary Speaker's first name.
     *
     * @param array $objects
     * @return void
     */
    public function sort(array $objects)
    {
        usort($objects, function ($obj1, $obj2) {
            return strcasecmp(
                $obj1->getPrimarySpeaker()->getFirstName(),
                $obj2->getPrimarySpeaker()->getFirstName()
            );
        });

        return $objects;
    }
}
