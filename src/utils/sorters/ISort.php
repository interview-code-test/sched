<?php
/**
 * ISort.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Utils\Sorters;

use SCHEDORG\Models\Event;

/**
 * Sort Interface
 *
 * @package     schedorg/chip
 * @subpackage  utils/sorters
 */
interface ISort
{
    public function sort(array $objects);
}
