<?php
/**
 * DateUtils.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Utils;

/**
 * Date Utility.
 *
 * @package     schedorg/chip
 * @subpackage  utils
 */
class NameUtils
{
    /**
     * Splits a full name into first, middle, and last name. Only dash and apostrophe is allowed
     * in any part of the full name. When the full name has more than 3 words, the following rule applies:
     * 1. First part is first name
     * 2. If third part exists, the second part is middle name
     * 3. If there are more than 3 words, see #1 and #2 above. Last part is last name. The rest
     *    is omitted.
     *
     * @param string $name
     * @return array
     */
    public static function split(string $name)
    {
        $parts = explode(' ', $name);

        // Allow only character, dash, and apostrophe.
        $parts = preg_replace("/[^a-zA-Z'\-]+/", '', $parts);

        array_walk($parts, 'trim');

        $sum = count($parts);

        if (1 === $sum) {
            $parts[1] = $parts[2] = '';
        } elseif (2 === $sum) {
            $lastName = array_pop($parts);
            array_push($parts, '', $lastName);
        } elseif ($sum > 3) {
            $lastName = array_pop($parts);
            $parts[2] = $lastName;
            $parts = array_slice($parts, 0, 3);
        }

        return $parts;
    }
}
