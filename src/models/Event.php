<?php
/**
 * Event.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Models;

/**
 * Event Model
 *
 * Event has 1 to many Sessions.
 *
 * @package     schedorg/chip
 * @subpackage  models
 */
class Event
{
    /**
     * Sessions of the Event.
     *
     * @var array An array of Event's Sessions
     */
    private $sessions;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Adds a Session into this Event.
     *
     * @param Session $session
     * @return self
     */
    public function addSession(Session $session)
    {
        $this->sessions[$session->getId()] = $session;

        return $this;
    }

    /**
     * Retrieves Sessions.
     *
     * @return void
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Adds a Speaker into a specific Session denoted by the Session Id.
     *
     * @param string $sessionId
     * @param User $user
     * @return self
     */
    public function addSpeaker($sessionId, User $user)
    {
        if ($this->exists($sessionId)) {
           $session = $this->sessions[$sessionId];
           $session->addSpeaker($user);
        }

        return $this;
    }

    /**
     * Retrieves all Speakers of a Session.
     *
     * @param string $sessionId
     * @return array
     */
    public function getSpeakers($sessionId)
    {
        $speakers = [];

        if ($this->exists($sessionId)) {
            $session = $this->sessions[$sessionId];
            $speakers = $session->getSpeakers();
        }

        return $speakers;
    }

    /**
     * Checks if a Session exists in the Event.
     *
     * @param string $sessionId
     * @return bool
     */
    public function exists($sessionId)
    {
        return !empty($this->sessions[$sessionId]);
    }
}
