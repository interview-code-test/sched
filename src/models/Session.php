<?php
/**
 * Session.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Models;

use \DateTime;

/**
 * Session Model
 *
 * A Session of an Event. It has 1 to many Speakers and other information.
 *
 * @package     schedorg/chip
 * @subpackage  controllers
 */
class Session
{
    /**
     * Session Id. Hash key.
     *
     * @var string
     */
    private $id;

    /**
     * Session name.
     *
     * @var string
     */
    private $name;

    /**
     * Session start time.
     *
     * @var DateTime
     */
    private $startDate;

    /**
     * Session end time.
     *
     * @var DateTime
     */
    private $endDate;

    /**
     * List of Speakers of this Session.
     *
     * @var array
     */
    private $speakers;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Sets Session Id.
     *
     * @param string $sessionId
     * @return self
     */
    public function setId(string $sessionId)
    {
        $this->id = $sessionId;

        return $this;
    }

    /**
     * Gets Session Id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets Session name.
     *
     * @param string $sessionName
     * @return self
     */
    public function setName(string $sessionName)
    {
        $this->name = $sessionName;

        return $this;
    }

    /**
     * Gets Session name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets Session start time.
     *
     * @param DateTime $startDate
     * @return self
     */
    public function setStartDate(DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Gets Session start time.
     *
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets Session end time.
     *
     * @param DateTime $endDate
     * @return self
     */
    public function setEndDate(DateTime $endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Gets Session end time.
     *
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Adds a Speaker into the list of Speakers of this Session.
     *
     * @param User $user
     * @return self
     */
    public function addSpeaker(User $user)
    {
        $this->speakers[] = $user;

        return $this;
    }

    /**
     * Gets all the speakers of this Session.
     *
     * @return array
     */
    public function getSpeakers()
    {
        return $this->speakers;
    }

    /**
     * Gets the first Speaker of this Session.
     *
     * @return User|null
     */
    public function getPrimarySpeaker()
    {
        return !empty($this->speakers[0]) ? $this->speakers[0] : null;
    }
}
