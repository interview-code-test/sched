<?php
/**
 * User.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Models;

use SCHEDORG\Models\Role;

/**
 * User Model
 *
 * The User can have 1 to many Roles, such as, Speaker, Artist, etc.
 *
 * @package     schedorg/chip
 * @subpackage  controllers
 */
class User
{
    /**
     * User id.
     *
     * @var int
     */
    private $id;

    /**
     * List of Roles of this User.
     *
     * @var array
     */
    private $roles;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $middleName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Adds a Role to this User.
     *
     * @param Role $role
     * @return self
     */
    public function addRole(Role $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Retrieves all the Roles of this User.
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Checks if this User has a certain Role.
     *
     * @param string $roleName
     * @return boolean
     */
    public function hasRole($roleName)
    {
        if (!empty($this->roles)) {
            foreach ($this->roles as $role) {
                if ($role->getRole() === $roleName) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Sets User Id.
     *
     * @param integer $id
     * @return self
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $firstName
     * @return self
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $middleName
     * @return self
     */
    public function setMiddleName(string $middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $lastName
     * @return self
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }
}
