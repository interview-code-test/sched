<?php
/**
 * Role.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 */
namespace SCHEDORG\Models;

/**
 * Role Model.
 *
 * The role of a User.
 *
 * @package     schedorg/chip
 * @subpackage  models
 */
class Role
{
    /**
     * Name of a Role.
     *
     * @var string
     */
    private $role;

    /**
     * Constructor.
     *
     * @param string $role
     */
    public function __construct(string $role = '')
    {
        $this->role = $role;
    }

    /**
     * Sets the Role name.
     *
     * @param string $role
     * @return void
     */
    public function setRole(string $role)
    {
        $this->role = $role;

        return $this;
    }
    
    /**
     * Retrieves the Role name.
     *
     * @return void
     */
    public function getRole()
    {
        return $this->role;
    }
}
