<?php
/**
 * index.php
 *
 * @author    Chip Salim <chipsalim@gmail.com>
 * @copyright 2018 Sched Code Challenge
 * @see       https://github.com/schedorg/chip
 * @since     0.1
 * @since     0.1.1 Added Unit Tests
 */
use SCHEDORG\Controllers\Scheduler;
use SCHEDORG\Utils\Factories\EventFactory;

require __DIR__ . '/vendor/autoload.php';

define('SCHEDORG_DATA_DIR', __DIR__ . '/data/');

$scheduler = EventFactory::createScheduler();
$scheduler->run()
    ->out()
    // ->dump()
    // ->displayJSON()
    // ->render()
    ;
